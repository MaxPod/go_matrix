package entity
import (
	"strconv"
)

type MyObject struct {
	Id int
	Name string
	Value float32
	Property string
	Flag bool
}


type MatrixMyObject struct {
	Id []int
	Name []string
	Value []float32
	Property []string
	Flag []bool

	// служебные поля
	length int // количество строк 
}


// т.е. список объектов представлем в виде матрицы, где каждая колонка массив определенного типа
// | ID | Name | Value | Property | Flag |
// |=====================================|
// |  4 |   4  |   100 |    4     |  4   |
// |  3 | Макс |    10 |    М     | true |
// |  1 | Саня |    40 |    М     | true |
// |  2 | Дима |    30 |    М     | true |
// |  4 | Паша |    10 |    М     | true |
// |  5 | Ваня |    10 |    М     | true |
// в [0]  индексе будем хранить сумму столбца для числовых полей или количество элементов

// методы матрицы

// type MatrixMyObject interface {
// 	New() *matrixMyObject
// 	Add(MyObject) error
// 	Find(string, interface{}) int
// 	FindAll(string, interface{}) []int
// 	Get(int) (MyObject, error) 
// 	Delete(int) error
// 	String()
// }

func (m *MatrixMyObject) New() *MatrixMyObject {
	//m = make(matrixMyObject)
	m.Id = make([]int, 1)
	m.Name = make([]string, 1)
	m.Value = make([]float32,1)
	m.Property = make([]string,1)
	m.Flag = make([]bool,1)
	m.length = 0

	return m
}


// добавление.  Добавляет в конец массива
func (m *MatrixMyObject) Add(myObject MyObject) error {
	m.Id = append(m.Id, myObject.Id)
	m.Name = append(m.Name, myObject.Name)
	m.Value = append(m.Value, myObject.Value)
	m.Property = append(m.Property, myObject.Property)
	m.Flag = append(m.Flag, myObject.Flag)
	m.length += 1
	m.Value[0] += myObject.Value //  в валуе будем хранить сумму значений
	if m.Id[0] < myObject.Id { m.Id[0] = myObject.Id } // в  ID  будем хранить максимальне значение ID
	
	return nil
}

// поиск. Возвращает номер строки по значению поля
func (m *MatrixMyObject) Find(field string, value interface{}) int {

	return 1
}

// поиск. Возвращает  массив номеров строк по занчению поля
func (m *MatrixMyObject) FindAll(field string, value interface{}) []int {


	return []int{1,2,3,4,5}
}

//  получить объект по строке
func (m *MatrixMyObject) Get(row int) (MyObject, error) {
	var result MyObject
	result.Id = m.Id[row]
	result.Name = m.Name[row]
	result.Value = m.Value[row]
	result.Property = m.Property[row]
	result.Flag = m.Flag[row]

	return result, nil
}

//  Удалить объект по строке
func (m *MatrixMyObject) Delete(row int) error {
	return nil
}

//  Вывод матрицы на экран  метод типа
func (m MatrixMyObject) String() string {
	result := " | ID | Name | Value | Property | Flag | \n"
	result += " |----|------|-------|----------|------| \n"

	for i := 1; i <= m.length; i++ { 
		result += " |  "+strconv.Itoa(m.Id[i])+" | "+m.Name[i]+"  |  "+strconv.FormatFloat(float64(m.Value[i]),'f', -1, 32)+"  |   "+m.Property[i]+"    | "+strconv.FormatBool(m.Flag[i])+"|  \n"
	}
	
	return result
}

//  еще можно добавить сортировки по полю, сворачвание, итоги и т.п. методы



