package main

import (
	"./entity"
	"fmt"
)



func main() {

	var matrix entity.MatrixMyObject

	sanya := entity.MyObject{
		Id:1,
		Name:"San",
		Value:101,
		Property:"whi",
		Flag:false,
	}
	
	dima := entity.MyObject{
		Id:2,
		Name:"Dim",
		Value:150,
		Property:"bla",
		Flag:false,
	}

	max := entity.MyObject{
		Id:3,
		Name:"Max",
		Value:100,
		Property:"red",
		Flag:false,
	}

	pavel := entity.MyObject{
		Id:4,
		Name:"Pav",
		Value:120,
		Property:"blu",
		Flag:false,
	}

	ivan := entity.MyObject{
		Id:5,
		Name:"Ivn",
		Value:200,
		Property:"gre",
		Flag:false,
	}
	
	matrix.New()
	err := matrix.Add(sanya)
	err = matrix.Add(dima)
	err = matrix.Add(max)
	err = matrix.Add(pavel)
	err = matrix.Add(ivan)

	if err!=nil {
		fmt.Println(err)
		return
	}


	fmt.Println(matrix)

	row3, _ := matrix.Get(3)
	fmt.Println(" Третья строка: ",row3)

	itogi, _ := matrix.Get(0)

	fmt.Println(" Сумма значений Value:", itogi.Value)

	fmt.Println(" Максимальный Id:", itogi.Id)

	fmt.Println(" Flag=false:", matrix.FindAll("Flag",false))
	
	fmt.Println("Done!")

}